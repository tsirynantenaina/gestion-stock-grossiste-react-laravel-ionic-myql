# PROJET GESTION D'UN GROSSISTE DE RIZ REACT-LARAVEL-IONIC

## FRONT REACT

* installer nodes modules

```
npm install

```

* Lancer serveur:

```
npm start

```

## BACK LARAVEL

* Ouvrir le fichier .env
* Changer le nom de BD à celle de votre base de données
* Faire migration

```
php artisan migrate
```

* Lancer le serveur

```
php artisan server:start
```

## MOBILE IONIC

* installer nodes modules ionic

```
npm install

```

* Lancer serveur:

```
ionic serve

```
* ou


```
npm start

```