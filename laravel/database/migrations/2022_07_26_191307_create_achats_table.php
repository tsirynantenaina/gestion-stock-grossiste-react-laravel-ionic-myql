<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAchatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('achats', function (Blueprint $table) {
            $table->id();
            $table->integer('idCli');
            $table->integer('refAchat');
            $table->integer('idProd');
            $table->integer('quantiteAchat');
            $table->integer('totalAchat');
            $table->string('dateAchat');
            
            
            $table->timestamps();
            $table->foreign('idProd')->on('id')->references('produits');
            $table->foreign('idCli')->on('id')->references('clients');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produits' , function(Blueprint $table){
            $table->dropConstrainedForeign('idProd');
        });

        Schema::table('clients' , function(Blueprint $table){
            $table->dropConstrainedForeign('idCli');
        });


        Schema::dropIfExists('achats');
    }
}
