<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReaprovisersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reaprovisers', function (Blueprint $table) {
            $table->id();
            $table->integer("quantiteReap");
            $table->integer("totalReap");
            $table->string("dateReap");
            $table->integer('idProd');
            $table->timestamps();
            $table->foreign('idProd')->on('id')->references('produits');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reaprovisers' , function(Blueprint $table){
            $table->dropConstrainedForeign('idProd');
        });
        Schema::dropIfExists('reaprovisers');
    }
}
