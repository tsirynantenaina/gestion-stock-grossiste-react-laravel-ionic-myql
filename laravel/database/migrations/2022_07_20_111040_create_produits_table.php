<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produits', function (Blueprint $table) {
            $table->id();
            $table->string('designation');
            $table->integer('quantite');
            $table->integer('prix');
            $table->timestamps();
            $table->integer('idFrns');
            $table->foreign('idFrns')->on('id')->references('fournisseurs');
            
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produits' , function(Blueprint $table){
            $table->dropConstrainedForeign('idFrns');
        });
        Schema::dropIfExists('produits');
    }
}
