<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FournisseurController;
use App\Http\Controllers\ProduitController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ReaproviseController;
use App\Http\Controllers\AchatController;
/*
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


/*Route::middleware(['cors'])->group(function () {
    Route::post('/hogehoge', 'Controller@hogehoge');

});*/




//****************************************FOURNISSEUR***************************************************
Route::get('allFournisseur', [FournisseurController::class , 'allFournisseur' ]);
Route::get('getFournisseur/{id}', [FournisseurController::class , 'getFournisseur' ]);
Route::post('/addFournisseur', [FournisseurController::class , 'addFournisseur' ]);
Route::put('updateFournisseur/{id}', [FournisseurController::class , 'updateFournisseur' ]);
Route::delete('deleteFournisseur/{id}', [FournisseurController::class , 'deleteFournisseur' ]);

//***************************************PRODUIT********************************************************
Route::get('allProduit', [ProduitController::class , 'allProduit' ]);
Route::get('getProduit/{id}', [ProduitController::class , 'getProduit' ]);
Route::post('addProduit', [ProduitController::class , 'addProduit' ]);
Route::put('updateProduit/{id}', [ProduitController::class , 'updateProduit' ]);
Route::delete('deleteProduit/{id}', [ProduitController::class , 'deleteProduit' ]);

//**************************************Client********************************************************
Route::get('allClient', [ClientController::class , 'allClient' ]);
Route::get('getClient/{id}', [ClientController::class , 'getClient' ]);
Route::post('addClient', [ClientController::class , 'addClient' ]);
Route::put('updateClient/{id}', [ClientController::class , 'updateClient' ]);
Route::delete('deleteClient/{id}', [ClientController::class , 'deleteClient' ]);

//**************************************REAPROV********************************************************
Route::get('allReaprovise', [ReaproviseController::class , 'allReaprovise' ]);
Route::put('reaproviserProduit/{id}', [ReaproviseController::class , 'reaproviserProduit' ]);

//**************************************ACHAT********************************************************
Route::put('addPanier/{id}', [AchatController::class , 'addPanier' ]);
Route::get('dansPanier', [AchatController::class , 'dansPanier' ]);
Route::put('validerAchat/{id}', [AchatController::class , 'validerAchat' ]);
Route::get('allAchat', [AchatController::class , 'allAchat' ]);
