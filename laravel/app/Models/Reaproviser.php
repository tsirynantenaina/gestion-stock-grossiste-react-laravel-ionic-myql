<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reaproviser extends Model
{
    use HasFactory;

    public $timestamp = false;
    public $fillable = ['idProd' , 'quantiteReap' , 'totalReap' , 'dateReap'];
}
