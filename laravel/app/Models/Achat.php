<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Achat extends Model
{
    use HasFactory;
    public $timestamp = false;
    public $fillable = ['idCli' ,'refAchat', 'idProd' , 'quantinteAchat' , 'totalAchat' , 'dateAchat'];
}
