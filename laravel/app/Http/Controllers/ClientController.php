<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Client;

class ClientController extends Controller
{
    //All client
    public function allClient(){
        $client= Client::all();
        return response()->json([
            'status' => 200 , 
            'clients' => $client
        ]);
    }

    //getClient
    public function getClient($id){
        $client = Client::find($id);
        return response()->json([
            'status' => 200 , 
            'client' => $client
        ]);
    }



    //add  Client
    public function addClient(Request $request){
        $client = new Client();
        $client->nomCli = $request->input('nomCli');
        $client->adresseCli = $request->input('adresseCli');
        $client->telCli = $request->input('telCli');
        $client->save();
        return response()->json([
            'status' => 200 , 
            'clients' =>  Client::all() 

        ]);
    }

    //update client
    public function updateClient(Request $request , $id){
        $client = Client::find($id);
        $client->nomCli = $request->input('nomCli');
        $client->adresseCli = $request->input('adresseCli');
        $client->telCli = $request->input('telCli');
        $client->update();
        $client->update();
         return response()->json([
            'status' => 200 , 
            'clients' =>  Client::all() 
        ]);
    }

    //delete client
    public function deleteClient(Request $request , $id){
        $client = Client::find($id);
        $client->delete();
         return response()->json([
            'status' => 200 , 
            'clients' =>  Client::all() 
        ]);
    }
}
