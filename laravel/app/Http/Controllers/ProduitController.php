<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produit;
use App\Models\Fournisseur;


class ProduitController extends Controller
{


    //All produit
    public function allProduit(){
        //select produits.* , fournisseurs.nomFrns from produits inner join fournisseurs on produits.idFrns = fournisseurs.id
        $produit= Produit::join('fournisseurs', 
                                'produits.idFrns', '=' , 'fournisseurs.id')
                            ->get(['produits.*', 'fournisseurs.nomFrns']); 
        return response()->json([
            'status' => 200 , 
            'produits' => $produit , 
            'fournisseurs' => Fournisseur::all(),
        ]);
    }

    public function getProduit($id){
        //select produits.* , fournisseurs.nomFrns from produits inner join fournisseurs on produits.idFrns = fournisseurs.id
        $produit= Produit::join('fournisseurs', 
                                'produits.idFrns', '=' , 'fournisseurs.id')
                            ->get(['produits.*', 'fournisseurs.nomFrns']) 
                            ->find($id); 
        return response()->json([
            'status' => 200 , 
            'produit' => $produit , 
        ]);
    }


    //add  produit
    public function addProduit(Request $request){
        $produit = new Produit();
        $produit->designation = $request->input('designation');
        $produit->idFrns = $request->input('idFrns');
        $produit->quantite = $request->input('quantite');
        $produit->prix = $request->input('prix');
        $produit->save();
        return response()->json([
            'status' => 200 , 
            'produits' => Produit::join('fournisseurs', 
                                'produits.idFrns', '=' , 'fournisseurs.id')
                            ->get(['produits.*', 'fournisseurs.nomFrns']), 
            
        ]);
    }

    //Update  produit
    public function updateProduit(Request $request , $id){
        $produit = Produit::find($id);
        $produit->designation = $request->input('designation');
        $produit->idFrns = $request->input('idFrns');
        $produit->quantite = $request->input('quantite');
        $produit->prix = $request->input('prix');
        $produit->update();
        return response()->json([
            'status' => 200 , 
            'produits' => Produit::join('fournisseurs', 
                                'produits.idFrns', '=' , 'fournisseurs.id')
                            ->get(['produits.*', 'fournisseurs.nomFrns']), 
            
        ]);
    }

    //delete  produit
    public function deleteProduit(Request $request , $id){
        $produit = Produit::find($id);
        $produit->delete();
        return response()->json([
            'status' => 200 , 
            'produits' => Produit::join('fournisseurs', 
                                'produits.idFrns', '=' , 'fournisseurs.id')
                            ->get(['produits.*', 'fournisseurs.nomFrns']), 
            
        ]);
    }


}
