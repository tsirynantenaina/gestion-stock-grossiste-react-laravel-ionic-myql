<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Fournisseur;

class FournisseurController extends Controller
{
    //All Fournisseurs
    public function allFournisseur(){
        $fournisseur= Fournisseur::all();
        return response()->json([
            'status' => 200 , 
            'fournisseurs' => $fournisseur
        ]);
    }

    //getFournisseursById
    public function getFournisseur($id){
        $fournisseur = Fournisseur::find($id);
        return response()->json([
            'status' => 200 , 
            'fournisseur' => $fournisseur
        ]);
    }

    //add  Fourniseur
    public function addFournisseur(Request $request){
        $fournisseur = new Fournisseur();
        $fournisseur->nomFrns = $request->input('nomFrns');
        $fournisseur->adresseFrns = $request->input('adresseFrns');
        $fournisseur->telFrns = $request->input('telFrns');
        $fournisseur->save();
        return response()->json([
            'status' => 200 , 
            'fournisseurs' =>  Fournisseur::all() 

        ]);
    }

    //update Fourniseur
    public function updateFournisseur(Request $request , $id){
        $fournisseur = Fournisseur::find($id);
        $fournisseur->nomFrns = $request->input('nomFrns');
        $fournisseur->adresseFrns = $request->input('adresseFrns');
        $fournisseur->telFrns = $request->input('telFrns');
        $fournisseur->update();
         return response()->json([
            'status' => 200 , 
            'fournisseurs' =>  Fournisseur::all() 
        ]);
    }

    //delete Fournisseur
    public function deleteFournisseur(Request $request , $id){
        $fournisseur = Fournisseur::find($id);
        $fournisseur->delete();
         return response()->json([
            'status' => 200 , 
            'fournisseurs' =>  Fournisseur::all() 
        ]);
    }
}
 