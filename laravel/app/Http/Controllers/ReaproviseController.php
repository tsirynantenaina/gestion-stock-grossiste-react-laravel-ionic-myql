<?php

namespace App\Http\Controllers;
use App\Models\Produit;
use App\Models\Fournisseur;
use App\Models\Reaproviser;
use Carbon\Carbon;

use Illuminate\Http\Request;

class ReaproviseController extends Controller

{

   //All reaprovision
    public function allReaprovise(){
        //select produits.* , fournisseurs.nomFrns from produits inner join fournisseurs on produits.idFrns = fournisseurs.id
        $reaprov= Reaproviser::join('produits', 
                                'produits.id', '=' , 'reaprovisers.idProd')
                            ->get(['produits.*', 'reaprovisers.*']) ;
        return response()->json([
            'status' => 200 , 
            'reaprovises' => $reaprov , 
        ]);
    }




    //reaprovisionner produit
    public function reaproviserProduit(Request $request , $id){
        $produit = Produit::find($id);
        $reap= new Reaproviser();


        $prix = $produit->prix; 
        $stock = $produit->quantite; 
        $quantiteReap = $request->input('quantiteReap');
        $totalReap = $prix * $quantiteReap;


        //add provision
        $date=  Carbon::now();
        $reap->idProd= $id;
        $reap->quantiteReap = $quantiteReap;
        $reap->dateReap = $date ; 
        $reap->totalReap = $totalReap;
        $reap->save();


        //modification quantité en stock
        $newStock = $stock + $quantiteReap ; 
        $produit->quantite = $newStock ;
        $produit->update();
        return response()->json([
            'status' => 200 , 
            'produits' => Produit::join('fournisseurs', 
                                'produits.idFrns', '=' , 'fournisseurs.id')
                            ->get(['produits.*', 'fournisseurs.nomFrns']),
            'reaprovises' => Reaproviser::join('produits', 
                                'produits.id', '=' , 'reaprovisers.idProd')
                            ->get(['produits.*', 'reaprovisers.*']) , 
            
        ]);
    }



    
}
