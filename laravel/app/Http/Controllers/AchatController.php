<?php

namespace App\Http\Controllers;
use App\Models\Produit;
use App\Models\Fournisseur;
use App\Models\Reaproviser;
use App\Models\Achat;
use Carbon\Carbon;

use Illuminate\Http\Request;

class AchatController extends Controller
{


     public function dansPanier(){
        $sac =   Achat::where('idCli' , '=' , '0')->sum('quantiteAchat');
        $somme = Achat::where('idCli' , '=' , '0')->sum('totalAchat');
        $paniers= Achat::join('produits', 
                                'produits.id', '=' , 'achats.idProd')
                            ->where('achats.idCli','0' )
                            ->get(['produits.designation', 'achats.*']) ;
        return response()->json([
            'status' => 200 , 
            'paniers' => $paniers , 
            'somme' => $somme ,
            'sac' => $sac , 
        ]);
    }


    public function addPanier(Request $request , $id){
        $achat = new Achat();
        $produit = Produit::find($id);

        $date=  Carbon::now();
        $quantiteAchat = $request->input('quantiteAchat');        
        $prix= $produit->prix ; 
        $stock = $produit->quantite; 
        $totalAchat = $prix * $quantiteAchat ; 


        $achat->idProd= $id ; 
        $achat->idCli = "0"; 
        $achat->dateAchat = $date ;
        $achat->quantiteAchat = $quantiteAchat;
        $achat->totalAchat = $totalAchat;
        $achat->refAchat = "0";
        $achat->save();

        $newStock = $stock - $quantiteAchat ; 
        $produit->quantite = $newStock ; 
        $produit->update(); 

        return response()->json([
            'status' => 200 , 
            'produits' => Produit::join('fournisseurs', 
                                'produits.idFrns', '=' , 'fournisseurs.id')
                            ->get(['produits.*', 'fournisseurs.nomFrns']),

            'paniers' => Achat::join('produits', 
                                'produits.id', '=' , 'achats.idProd')
                            ->where('achats.idCli','0' )
                            ->get(['produits.designation', 'achats.*']) ,
            'somme' => Achat::where('idCli' , '=' , '0')->sum('totalAchat'), 
            
        ]);

    }

     public function validerAchat(Request $request , $id){
        $maxRef= Achat::max('refAchat');
        $nouveauRef= $maxRef + 1 ; 
        $achat = Achat::where('idCli' , '0')
                        ->update(['idCli' => $id , 'refAchat' => $nouveauRef ]);
        $somme = Achat::where('idCli' , '=' , '0')->sum('totalAchat');
        return response()->json([
            'status' => 200 , 
            'paniers' => Achat::all() , 
            'produits' => Produit::join('fournisseurs', 
                                'produits.idFrns', '=' , 'fournisseurs.id')
                            ->get(['produits.*', 'fournisseurs.nomFrns']),

            'paniers' => Achat::join('produits', 
                                'produits.id', '=' , 'achats.idProd')
                            ->where('achats.idCli','0' )
                            ->get(['produits.designation', 'achats.*']) ,
            'somme' => $somme ,
        ]);


     }

      public function allAchat(){
       $achat = Achat::all()->groupBy('refAchat');
        return response()->json([
            'status' => 200 , 
            'achat' => $achat , 
    
        ]);
    }
}
