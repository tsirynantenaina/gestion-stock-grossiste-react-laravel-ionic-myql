import React, { Component } from "react";
import {Switch , Route} from 'react-router-dom';
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Sidebar from './components/Sidebar';
import Topbar from './components/Topbar';

import Acceuil from './components/Acceuil';
import Fournisseur from './components/Fournisseur';
import Produit from './components/Produit';
import Client from './components/Client';
import Reaprovise from './components/Reaprovise';
import Achat from './components/Achat';

class App extends Component {
  render() {
    return (
        <div id="wrapper">
          <Sidebar />
          <div id="content-wrapper">
              <div id="content">
                  <Topbar />
                  <div class="container-fluid">
                    <Switch>
                      <Route exact path="/" component={Acceuil} />
                      <Route path="/fournisseur" component={Fournisseur} />
                      <Route path="/produit" component={Produit} />
                      <Route path="/client" component={Client} />
                      <Route path="/reaprovise" component={Reaprovise} />
                      <Route path="/achat" component={Achat} />
                   </Switch>
                  </div>
              </div>
          </div>
        </div>
    );
  }
}

export default App;
