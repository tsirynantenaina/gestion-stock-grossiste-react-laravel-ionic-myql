import React, { Component}  from "react";
import axios from  "axios"; 
import toastr from 'cogo-toast';

export default class Achat extends Component {

  //<tr sel>{ item.id === 1 ? <p>Hi</p> : <p>Bey </p>}</tr>
  

  state = {
     idProd:"",
     quantiteAchat:"",
     designation:"",
     idCli:"",

     produits: [],
     paniers:[],
     clients:[],
     somme:[],
     
     loading: true,
  }

  //relie name input avec name state
  handleInput = (e) =>{
    this.setState({
      [e.target.name]:e.target.value
    });

  }


addPanier = async (e , id) =>{
    e.preventDefault();
    const res = await axios.put('http://localhost:8000/api/addPanier/'+id, this.state);
    const resPanier = await axios.get('http://localhost:8000/api/dansPanier');
    //si le route laravel a  repondu
    if(res.data.status === 200){
      toastr.success('Produit ajouter au panier', {position : 'top-right', heading: 'Succes'});
      this.setState({
        idProd:"",
        designation:"",
        quantiteAchat:"", 
        produits:res.data.produits , 
        somme: resPanier.data.somme,
        paniers : res.data.paniers , 
        loading:false,
      })
    }

  }

  validerAchat = async (e , id) =>{
    e.preventDefault();
    id= this.state.idCli;
      const res = await axios.put('http://localhost:8000/api/validerAchat/'+id, this.state);
      //si le route laravel a  repondu
      if(res.data.status === 200){
        toastr.success('Achat bien valider', {position : 'top-right', heading: 'Succes'});
        this.setState({
          idProd:"",
          idCli:"",
         designation:"",
         quantiteAchat:"", 
         produits:res.data.produits , 
         paniers : res.data.paniers , 
         loading: false,
       })
     }
  }

 getProduitById = (e , id , design) =>{
     this.setState({
        idProd : id , 
        designation: design, 
      });
     
 }


   initializeState= (e)=> {
    e.preventDefault();
    this.setState({
        idProd:"",
        quantiteAchat:"",
      });
  }


  //All produit and fournisseur when page or component load 
  async componentDidMount(){
    const res = await axios.get('http://localhost:8000/api/allProduit');
    const resPanier = await axios.get('http://localhost:8000/api/dansPanier');
    const resClient= await axios.get('http://localhost:8000/api/allClient');

    //console.log(res);
    if(res.data.status === 200){
      this.setState({
        produits: res.data.produits,
        paniers: resPanier.data.paniers,
        somme: resPanier.data.somme,
        clients: resClient.data.clients,
        loading: false,
      });
        
    }
  }





  render() {
     var produit_HTML_TABLE="";
     var panier_HTML_TABLE="";
     var panierfoot_HTML_TABLE="";
     var client_HTML_OPTION ="";
      if( this.state.loading){
            produit_HTML_TABLE = <tr><td colspan="6">En attente ...</td></tr> ;  
            panierfoot_HTML_TABLE = <tr><td colspan="6">En attente ...</td></tr> ; 

      }else{
          produit_HTML_TABLE=
          this.state.produits.map((item) => {
              return(
                  <tr key={item.id}>
                      <td>{item.id}</td>
                      <td>{item.designation}</td>
                      <td>{item.nomFrns}</td>
                      <td>{item.quantite}</td>
                      <td>{item.prix}</td>
                      <td>
                          <a  class="btn btn-success"  onClick={(e)=> this.getProduitById(e, item.id , item.designation )} data-toggle="modal" data-target="#addModal" >acheter</a>
                      </td>

                  </tr>

              );
          });
          panier_HTML_TABLE=
          this.state.paniers.map((item) => {
              return(
                  <tr key={item.id}>
                     <td>{item.designation}</td>
                      <td>{item.quantiteAchat}</td>
                      <td>{item.totalAchat}</td>

                  </tr>

              );

          });
          
          panierfoot_HTML_TABLE = <tfoot><tr><td colspan="1">Somme : {this.state.somme} </td><td colspan="2"><button class="btn btn-primary" data-toggle="modal" data-target="#achatModal">Valider achat</button></td></tr></tfoot> ; 
          
          client_HTML_OPTION=

          this.state.clients.map((item) => {
              return(<option key={item.id}  value={item.id}> {item.nomCli} </option>);
            })
                    
   
      }



    return (

    <div>
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-600">Achat</h1>
      </div>


          <div class="row">
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Produits </h6>
                  
                </div>
                  <div class="card-body">
                      <div class="card-body">
                        <div class="table-responsive">
                          <table class="table table-bordered table-striped dataTable" id="dataTable" width="100%" cellSpacing="0">
                            <thead>
                              <tr>
                                <th>ID</th>
                                <th>Designation</th>
                                <th>Fournisseur</th>
                                <th>Stock</th>
                                <th>PU/sac</th>
                                <th>Actions</th>
                              </tr>
                            </thead>
                            
                            <tbody>
                                {produit_HTML_TABLE}
                            </tbody>
                          </table>
                        </div>
                      </div>
                   </div>                    
              </div>
            </div>


            <div class="col-xl-4 col-lg-5">
              <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Panier d'achat</h6>
                </div>
            
                <div class="card-body">
                  <div class="card-body">
                        <div class="table-responsive">
                          <table class="table table-bordered table-striped dataTable" id="dataTable" width="100%" cellSpacing="0">
                            <thead>
                              <tr>
                                <th>Produits</th>
                                <th>Quanite</th>
                                <th>Total</th>
                              </tr>
                            </thead>
                            
                            <tbody>
                                {panier_HTML_TABLE}
                            </tbody>
                            {panierfoot_HTML_TABLE}
                          </table>
                        </div>
                      </div>
                </div>
              </div>
            </div>
          </div>



        <div class="modal fade" id="addModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            
            <form>
              <div class="modal-body">
                <div><strong>Vous-voulez combien de sacs de : {this.state.designation} ? </strong></div>
                <input type="hidden" onChange={this.handleInput} name="idProd" value={this.state.idProd}  class="form-control" placeholder="Designation" required/> <br />                
                <input type="number" onChange={this.handleInput} name="quantiteAchat" value={this.state.quantiteAchat}  class="form-control" placeholder="Quantite à acheter" required/><br />
  
              </div>
              <div class="modal-footer">
                <button  class="btn btn-secondary" type="button" data-dismiss="modal">Retour</button>
                <button onClick={(e)=> this.addPanier(e,this.state.idProd)} class="btn btn-success" data-dismiss="modal"> Valider</button>
              </div>
            </form>
          </div>
        </div>
      </div>


      <div class="modal fade" id="achatModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Ajouter nouveau produit</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <form onSubmit={this.saveProduit}>
              <div class="modal-body">
                   <select  name="idCli" onChange={this.handleInput} value={this.state.idCli}  class="form-control" required >
                      <option selected>Choisir client</option>
                      {client_HTML_OPTION}
                </select><br />
  
              </div>
              <div class="modal-footer">
                <button  class="btn btn-secondary" type="button" data-dismiss="modal">Retour</button>
                <button onClick={(e)=> this.validerAchat(e,this.state.idCli)}  type="submit" class="btn btn-primary"  data-dismiss="modal" >Enregistrer</button>
              </div>
            </form>
          </div>
        </div>
      </div>

    </div>

    );
  }
}
