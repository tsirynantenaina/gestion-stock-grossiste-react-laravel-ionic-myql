import React, { Component , useState } from "react";
import axios from  "axios"; 
import toastr from 'cogo-toast';

export default class Fournissseur extends Component {



  state = {
     idFrns:"",
     nomFrns:"",
     adresseFrns :"",
     telFrns:"",

     fournisseurs: [],
     loading: true,
  }

  //relie name input avec name state
  handleInput = (e) =>{
    this.setState({
      [e.target.name]:e.target.value
    });

  }





  
  saveFournisseur = async (e) =>{
    e.preventDefault();
      const res = await axios.post('http://localhost:8000/api/addFournisseur', this.state);
      
    //si le route laravel a  repondu
    if(res.data.status === 200){
      toastr.success('Insertion bien reussie', {position : 'top-right', heading: 'Succes'});
      this.setState({
        nomFrns:"",
        adresseFrns:"",
        telFrns:"",
        fournisseurs: res.data.fournisseurs,
        loading:false ,
      })
      
    }
  }


  getFournisseurById = async (e , id , nom , adresse, tel) =>{
     this.setState({
        idFrns : id , 
        nomFrns:nom , 
        adresseFrns:adresse , 
        telFrns: tel , 
      });
 }

  initializeState= (e)=> {
    e.preventDefault();
    this.setState({
        idFrns : "" , 
        nomFrns: "", 
        adresseFrns:"" , 
        telFrns:"" , 
      });
  }

  updateFournisseur = async (e , id) =>{
    e.preventDefault();
      const res = await axios.put('http://localhost:8000/api/updateFournisseur/'+id, this.state);
      
    //si le route laravel a  repondu
    if(res.data.status === 200){
      toastr.success('Modification bien reussie', {position : 'top-right', heading: 'Succes'});
      this.setState({
        idFrns:"",
        nomFrns:"",
        adresseFrns:"",
        telFrns:"",
        fournisseurs: res.data.fournisseurs,
        loading:false ,
      })

    }
  }

  deleteFournisseur = async (e , id) =>{
    e.preventDefault();
      const res = await axios.delete('http://localhost:8000/api/deleteFournisseur/'+id);

    //si le route laravel a  repondu
    if(res.data.status === 200){
      toastr.error('Suppression bien reussie', {position : 'top-right', heading: 'Succes'});
      this.setState({
        idFrns:"",
        nomFrns:"",
        adresseFrns:"",
        telFrns:"",
        fournisseurs: res.data.fournisseurs,
        loading:false ,

      })
     
    }
  }





  



  //All founisseur when page or component load 
  async componentDidMount(){
    const res = await axios.get('http://localhost:8000/api/allFournisseur');
    //console.log(res);
    if(res.data.status === 200){
      this.setState({
        fournisseurs: res.data.fournisseurs,
        loading: false,
      });
        
    }

  }





  render() {

      var fournisseur_HTML_TABLE="";
      if( this.state.loading){
            fournisseur_HTML_TABLE = <tr><td colspan="5">En attente ...</td></tr> ; 

      }else{
          fournisseur_HTML_TABLE=
          this.state.fournisseurs.map((item) => {
              return(
                  <tr key={item.id}>
                      <td>{item.id}</td>
                      <td>{item.nomFrns}</td>
                      <td>{item.adresseFrns}</td>
                      <td>{item.telFrns}</td>
                      <td>
                          <button  class="btn btn-primary"  onClick={(e)=> this.getFournisseurById(e,  item.id, item.nomFrns , item.adresseFrns , item.telFrns)} data-toggle="modal" data-target="#editModal" ><i class="fa fa-edit"></i> </button>
                          <button  class="btn btn-danger"   onClick={(e)=> this.getFournisseurById(e,  item.id, item.nomFrns , item.adresseFrns , item.telFrns)} data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash"></i> </button>

                      </td>

                  </tr>

              );
          });

      }



    return (

    <div>
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-600">Fournissseur</h1>
        <a onClick={this.initializeState}  href="#" data-toggle="modal" data-target="#addModal" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" ><i class="fas fa-compress fa-sm text-white-100"></i> Nouveau fournisseur</a>
      </div>


        <div class="card shadow mb-4">

            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">liste fournisseur</h6>
            </div>

            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped dataTable" id="dataTable" width="100%" cellSpacing="0">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Nom</th>
                      <th>Adresse</th>
                      <th>Telephone</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  
                  <tbody>
                    {fournisseur_HTML_TABLE}

                  </tbody>
                </table>

              </div>
            </div>

        </div>


        <div class="modal fade" id="addModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Ajouter nouveau fournissuer</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <form onSubmit={this.saveFournisseur}>
              <div class="modal-body">
                <input type="text" onChange={this.handleInput} name="nomFrns" value={this.state.nomFrns}  class="form-control" placeholder="Nom du fournisseur" /> <br />
                <input type="text" onChange={this.handleInput} name="adresseFrns" value={this.state.adresseFrns}  class="form-control" placeholder="adresse du fournisseur" /><br />            
                <input type="text" onChange={this.handleInput} name="telFrns" value={this.state.telFrns}  class="form-control" placeholder="Telephone fournisseur" />
              </div>
              <div class="modal-footer">
                <button  class="btn btn-secondary" type="button" data-dismiss="modal">Retour</button>
                <button type="submit" class="btn btn-primary" >Enregistrer</button>
              </div>
            </form>
          </div>
        </div>
      </div>


      <div class="modal fade" id="editModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Ecition fournissuer</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <form>
              <div class="modal-body">
                <input type="text" onChange={this.handleInput} name="nomFrns" value={this.state.nomFrns}  class="form-control" placeholder="Nom du fournisseur" /> <br />
                <input type="text" onChange={this.handleInput} name="adresseFrns" value={this.state.adresseFrns}  class="form-control" placeholder="adresse du fournisseur" /><br />            
                <input type="text" onChange={this.handleInput} name="telFrns" value={this.state.telFrns}  class="form-control" placeholder="Telephone fournisseur" />
              </div>
              <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Retour</button>
                <button  onClick={(e)=> this.updateFournisseur(e,this.state.idFrns)} class="btn btn-primary" data-dismiss="modal">Enregistrer</button>
              </div>
            </form>
          </div>
        </div>
      </div>

       <div class="modal fade" id="deleteModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Suppression Fournisseur</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <form >
              <div class="modal-body">
                  <p>Voulez vous vraiment supprimer le <strong>  fournisseurs {this.state.nomFrns} , ID: {this.state.idFrns} </strong> ?<br/> Attention !!! Toutes attentions tous les données de ce fournisseurs vont etre supprimer </p>
              </div>
              <div class="modal-footer">
                <button  class="btn btn-secondary" type="button" data-dismiss="modal">Annuler</button>
                <button onClick={(e)=> this.deleteFournisseur(e,this.state.idFrns)}  class="btn btn-danger" data-dismiss="modal">Confirmer</button>
              </div>
            </form>
          </div>
        </div>
      </div>







    </div>

    );
  }
}
