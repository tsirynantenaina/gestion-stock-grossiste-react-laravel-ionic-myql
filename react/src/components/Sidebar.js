import React, { Component } from "react";
import { Link } from "react-router-dom";


export default class Produit extends Component {


 

  render() {

    return (

    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">E-VARY<sup>2</sup></div>
      </a>

      <hr class="sidebar-divider my-0"/>


      <li class="nav-item">
        <Link to={"/"} class="nav-link">
          <i class="fas fa-fw fa-home"></i>
          <span>Acceuil</span>

        </Link>
      </li>


      <li class="nav-item">
        <Link to={"/fournisseur"} class="nav-link">
          <i class="fas fa-fw fa-inbox"></i>
          <span>Fournissuer</span>

        </Link>
      </li>

      <li class="nav-item">
        <Link to={"/produit"} class="nav-link">
          <i class="fas fa-fw fa-table"></i>
          <span>Produit</span>

        </Link>
      </li>

      <li class="nav-item">
        <Link to={"/client"} class="nav-link">
          <i class="fas fa-fw fa-users"></i>
          <span>Client</span>
        </Link>
      </li>

      <li class="nav-item">
        <Link to={"/reaprovise"} class="nav-link">
          <i class="fas fa-fw fa-plus"></i>
          <span> Reaprovisionnement</span>
        </Link>
      </li>

      <li class="nav-item">
        <Link to={"/achat"} class="nav-link">
          <i class="fas fa-fw fa-chart-area"></i>
          <span> Achat </span>
        </Link>
      </li>
      <hr class="sidebar-divider d-none d-md-block" />
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>
    </ul>

    


    );
  }
}
