import React, { Component}  from "react";
import axios from  "axios"; 
import toastr from 'cogo-toast';

export default class Produit extends Component {

  //<tr sel>{ item.id === 1 ? <p>Hi</p> : <p>Bey </p>}</tr>
  

  state = {
     idProd:"",
     designation:"",
     idFrns:"",
     nomFrns:"",
     quantite :"",
     prix:"",

     fournisseurs:[],
     produits: [],
     loading: true,
  }

  //relie name input avec name state
  handleInput = (e) =>{
    this.setState({
      [e.target.name]:e.target.value
    });

  }





  
  saveProduit = async (e) =>{
    e.preventDefault();
    const res = await axios.post('http://localhost:8000/api/addProduit', this.state);
    //si le route laravel a  repondu
    if(res.data.status === 200){
      toastr.success('Insertion bien reussie', {position : 'top-right', heading: 'Succes'});
      this.setState({
        designation:"",
        quantite:"",
        prix:"",
        idFrns:"",
        produits: res.data.produits,
        loading:false ,
      })
      
    }
  }


  getProduitById = (e , id , design, fournisseur_nom, fournisseur_id ,  qte , pu) =>{
     this.setState({
        idProd : id , 
        designation: design , 
        idFrns: fournisseur_id , 
        nomFrns : fournisseur_nom , 
        quantite: qte, 
        prix : pu , 
      });
     
 }


 updateProduit = async (e , id) =>{
    e.preventDefault();
    const res = await axios.put('http://localhost:8000/api/updateProduit/'+id, this.state);
    //si le route laravel a  repondu
    if(res.data.status === 200){
      toastr.success('Modification bien reussie', {position : 'top-right', heading: 'Succes'});
      this.setState({
        idProd:"",
        designation:"",
        quantite:"",
        prix:"",
        idFrns:"",
        nomFrns:"",
        produits: res.data.produits,
        loading:false ,
      })
      
    }
  }

   initializeState= (e)=> {
    e.preventDefault();
    this.setState({
        idProd:"",
        designation:"",
        quantite:"",
        prix:"",
        idFrns:"",
        nomFrns:"", 
      });
  }

  deleteProduit = async (e , id) =>{
    e.preventDefault();
    const res = await axios.delete('http://localhost:8000/api/deleteProduit/'+id);
    //si le route laravel a  repondu
    if(res.data.status === 200){
      toastr.error('Suppression bien reussie', {position : 'top-right', heading: 'Succes'});
      this.setState({
        idProd:"",
        designation:"",
        quantite:"",
        prix:"",
        idFrns:"",
        nomFrns:"",
        produits: res.data.produits,
        loading:false ,
      })
      
    }
  }




  



  //All produit and fournisseur when page or component load 
  async componentDidMount(){
    const res = await axios.get('http://localhost:8000/api/allProduit');
    //console.log(res);
    if(res.data.status === 200){
      this.setState({
        produits: res.data.produits,
        fournisseurs: res.data.fournisseurs, 
        loading: false,
      });
        
    }
  }





  render() {
     var produit_HTML_TABLE="";
     var fournisseur_HTML_OPTION="";
      if( this.state.loading){
            produit_HTML_TABLE = <tr><td colspan="6">En attente ...</td></tr> ; 

      }else{
          produit_HTML_TABLE=
          this.state.produits.map((item) => {
              return(
                  <tr key={item.id}>
                      <td>{item.id}</td>
                      <td>{item.designation}</td>
                      <td>{item.nomFrns}</td>
                      <td>{item.quantite}</td>
                      <td>{item.prix}</td>
                      <td>
                          <button  class="btn btn-primary"  onClick={(e)=> this.getProduitById(e,  item.id, item.designation , item.nomFrns , item.idFrns,  item.quantite , item.prix)} data-toggle="modal" data-target="#editModal" ><i class="fa fa-edit"></i> </button>
                          <button  class="btn btn-danger"  onClick={(e)=> this.getProduitById(e,  item.id, item.designation , item.nomFrns , item.idFrns,  item.quantite , item.prix)} data-toggle="modal" data-target="#deleteModal" ><i class="fa fa-trash"></i> </button>
                          </td>

                  </tr>

              );
          });

      }

       fournisseur_HTML_OPTION=
          this.state.fournisseurs.map((item) => {
              return(
                <option key={item.id} value={item.id}>{item.nomFrns}</option>
              );
          });






    return (

    <div>
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-600">Produit</h1>
        <a onClick={this.initializeState}  href="#" data-toggle="modal" data-target="#addModal" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" ><i class="fas fa-compress fa-sm text-white-100"></i> Nouveau produit</a>
      </div>


        <div class="card shadow mb-4">

            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Liste des produit disponible</h6>
            </div>

            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped dataTable" id="dataTable" width="100%" cellSpacing="0">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Designation</th>
                      <th>Fournisseur</th>
                      <th>Quantité(en sac)</th>
                      <th>PU/sac</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  
                  <tbody>
                      {produit_HTML_TABLE}
                  </tbody>
                </table>

              </div>
            </div>

        </div>


        <div class="modal fade" id="addModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Ajouter nouveau produit</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <form onSubmit={this.saveProduit}>
              <div class="modal-body">
                <input type="text" onChange={this.handleInput} name="designation" value={this.state.designation}  class="form-control" placeholder="Designation" required/> <br />
                <select onChange={this.handleInput}  name="idFrns"   class="form-control" required >
                      <option selected disabled >Veuillez choisir le fournisseur</option>
                      {fournisseur_HTML_OPTION}
                </select><br />
                <input type="number" onChange={this.handleInput} name="quantite" value={this.state.quantite}  class="form-control" placeholder="Quantite (en sac)" required/><br />
                <input type="number" onChange={this.handleInput} name="prix" value={this.state.prix}  class="form-control" placeholder="Prix/sac" required/>
            
              </div>
              <div class="modal-footer">
                <button  class="btn btn-secondary" type="button" data-dismiss="modal">Retour</button>
                <button type="submit" class="btn btn-primary" >Enregistrer</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="modal fade" id="editModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Ajouter nouveau produit</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <form onSubmit={this.saveProduit}>
              <div class="modal-body">
                <input type="text" name="designation" onChange={this.handleInput} value={this.state.designation}  class="form-control" placeholder="Designation" required/> <br />
                <select  name="idFrns" onChange={this.handleInput}   class="form-control" required >
                      <option selected value={this.state.idFrns}>{this.state.nomFrns}</option>
                      <option disabled >---------------------Changer le fournisseur----------------------</option>
                      {fournisseur_HTML_OPTION}
                </select><br />
                <input type="number" onChange={this.handleInput} name="quantite" value={this.state.quantite}  class="form-control" placeholder="Quantite (en sac)" required/><br />
                <input type="number" onChange={this.handleInput} name="prix" value={this.state.prix}  class="form-control" placeholder="Prix/sac" required/>
            
              </div>
              <div class="modal-footer">
                <button  class="btn btn-secondary" type="button" data-dismiss="modal">Retour</button>
                <button onClick={(e)=> this.updateProduit(e,this.state.idProd)}  type="submit" class="btn btn-primary"  data-dismiss="modal" >Enregistrer</button>
              </div>
            </form>
          </div>
        </div>
      </div>


       <div class="modal fade" id="deleteModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Suppression Fournisseur</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <form >
              <div class="modal-body">
                  <p>Voulez vous vraiment supprimer le <strong>  produit {this.state.designation} , ID: {this.state.idProd} </strong> ?<br/> Attention !!! Toutes attentions tous les données de ce produit vont etre supprimer </p>
              </div>
              <div class="modal-footer">
                <button  class="btn btn-secondary" type="button" data-dismiss="modal">Annuler</button>
                <button onClick={(e)=> this.deleteProduit(e,this.state.idProd)}  class="btn btn-danger" data-dismiss="modal">Confirmer</button>
              </div>
            </form>
          </div>
        </div>
      </div>








    </div>

    );
  }
}
