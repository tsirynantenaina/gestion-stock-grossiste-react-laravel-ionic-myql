import React, { Component}  from "react";
import axios from  "axios"; 
import toastr from 'cogo-toast';

 

export default class Reaprovise extends Component {

 state = {
     idProd:"",
     quantiteReap:"",

     reaprovises:[],
     produits: [],
     loading: true,
  }

   //relie name input avec name state
  handleInput = (e) =>{
    this.setState({
      [e.target.name]:e.target.value
    });

  }

  reaproviserProduit = async (e , id) =>{
    e.preventDefault();
    const res = await axios.put('http://localhost:8000/api/reaproviserProduit/'+id, this.state);
    //si le route laravel a  repondu
    if(res.data.status === 200){
      toastr.success('Reaprovisonnement bien reussie', {position : 'top-right', heading: 'Succes'});
      this.setState({
        quantiteReap:"",
        reaprovises: res.data.reaprovises,
        produits: res.data.produits,
        loading:false ,
      })

      console.log(this.state.idProd+""+this.state.quantiteReap);
      
    }
  }

//All produit and fournisseur when page or component load 
  async componentDidMount(){
    const res = await axios.get('http://localhost:8000/api/allProduit');
    const resReaprov = await axios.get('http://localhost:8000/api/allReaprovise');
    //console.log(res);
    if(res.data.status === 200){
      this.setState({
        produits: res.data.produits,
        loading: false,
      });   
    }

    if(resReaprov.data.status === 200){
      this.setState({
        reaprovises: resReaprov.data.reaprovises,
        loading: false,
      });
    }

  }


  render() {
    var produit_liste="";
    var reaprov_HTML_TABLE ="";
      if( this.state.loading){
            produit_liste = <option>loading ....</option> ; 
            reaprov_HTML_TABLE = <tr > <td colspan="6"> En attente... </td></tr>

      }else{
          produit_liste=
          this.state.produits.map((item) => {
              return(
                  <option key={item.id} value={item.id}>{item.designation} fournis par  {item.nomFrns}, stock : {item.quantite} sacs</option>
              );
          });


          reaprov_HTML_TABLE=
          this.state.reaprovises.map((item) => {
               return(
                  <tr key={item.id}>
                      <td>{item.id}</td>
                      <td>{item.designation}</td>
                      <td>{item.quantiteReap} sacs</td>
                      <td>le {item.dateReap} </td>
                      <td>{item.totalReap}</td>
                  </tr>

              );
          });

      }

    return (
        <div>
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
               <h1 class="h3 mb-0 text-gray-600">Reaprovisonnement</h1>
                  <a onClick={this.initializeState}  href="#" data-toggle="modal" data-target="#addModal" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" ><i class="fas fa-plus fa-sm text-white-100"></i> Réaprovisonner un produit </a>
              </div>


              <div class="card shadow mb-4">

                  <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Liste des produit disponible</h6>
                  </div>

                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped dataTable" id="dataTable" width="100%" cellSpacing="0">
                        <thead>
                          <tr>
                            <th>N°</th>
                            <th>Designation</th>
                            <th>Provison</th>
                            <th>Derrnier reaprovison</th>
                            <th>Total en Ariary</th>
                          </tr>
                        </thead>
                        
                        <tbody>
                            {reaprov_HTML_TABLE}

                        </tbody>
                      </table>

                    </div>
                  </div>

              </div>



               <div class="modal fade" id="addModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Reaprovisionner un produit</h5>
                      <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <form >
                      <div class="modal-body">
                         <select onChange={this.handleInput}  name="idProd"   class="form-control" required >
                              <option selected disabled >-----------Choisir le produit à reaprovisionner---------</option>
                              {produit_liste}
                        </select><br />
                        <input type="number" onChange={this.handleInput} name="quantiteReap" value={this.state.quantiteReap}  class="form-control" placeholder="Quantite à reaprovisonner (en sac)" required/><br />
                      </div>
                      <div class="modal-footer">
                        <button  class="btn btn-secondary" type="button" data-dismiss="modal">Retour</button>
                        <button onClick={(e)=> this.reaproviserProduit(e,this.state.idProd)} type="submit" class="btn btn-primary" >Enregistrer</button>
                      </div >
                    </form>
                  </div>
                </div>
              </div>



        </div>
    );
  }
}
