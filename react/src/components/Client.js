import React, { Component } from "react";
import axios from  "axios"; 
import toastr from 'cogo-toast';

export default class Fournissseur extends Component {


  state = {
     idCli:"",
     nomCli:"",
     adresseCli :"",
     telCli:"",

     clients: [],
     loading: true,
  }

  //relie name input avec name state
  handleInput = (e) =>{
    this.setState({
      [e.target.name]:e.target.value
    });

  }





  
  saveClient = async (e) =>{
    e.preventDefault();
      const res = await axios.post('http://localhost:8000/api/addClient', this.state);
      
    //si le route laravel a  repondu
    if(res.data.status === 200){
      toastr.success('Insertion bien reussie', {position : 'top-right', heading: 'Succes'});
      this.setState({
        nomCli:"",
        adresseCli:"",
        telCli:"",
        clients: res.data.clients,
        loading:false ,
      })
      
    }
  }


   getClientById = async (e , id , nom , adresse, tel) =>{
     this.setState({
        idCli : id , 
        nomCli:nom , 
        adresseCli:adresse , 
        telCli: tel , 
      });
 }

  updateClient = async (e , id) =>{
    e.preventDefault();
      const res = await axios.put('http://localhost:8000/api/updateClient/'+id, this.state);
      
    //si le route laravel a  repondu
    if(res.data.status === 200){
      toastr.success('Modification bien reussie', {position : 'top-right', heading: 'Succes'});
      this.setState({
        idCli:"",
        nomCli:"",
        adresseCli:"",
        telCli:"",
        clients: res.data.clients,
        loading:false ,
      })

    }
  }

  initializeState= (e)=> {
    e.preventDefault();
    this.setState({
        idCli : "" , 
        nomCli: "", 
        adresseCli:"" , 
        telCli:"" , 
      });
  }

  deleteClient = async (e , id) =>{
    e.preventDefault();
      const res = await axios.delete('http://localhost:8000/api/deleteClient/'+id);

    //si le route laravel a  repondu
    if(res.data.status === 200){
      toastr.error('Suppression bien reussie', {position : 'top-right', heading: 'Succes'});
      this.setState({
        idCli:"",
        nomCli:"",
        adresseCli:"",
        telCli:"",
        clients: res.data.clients,
        loading:false ,

      })
     
    }
  }





  



  //All founisseur when page or component load 
  async componentDidMount(){
    const res = await axios.get('http://localhost:8000/api/allClient');
    //console.log(res);
    if(res.data.status === 200){
      this.setState({
        clients: res.data.clients,
        loading: false,
      });
        
    }

  }





  render() {

      var client_HTML_TABLE="";
      if( this.state.loading){
            client_HTML_TABLE = <tr><td colspan="5">En attente ...</td></tr> ; 

      }else{
          client_HTML_TABLE=
          this.state.clients.map((item) => {
              return(
                  <tr key={item.id}>
                      <td>{item.id}</td>
                      <td>{item.nomCli}</td>
                      <td>{item.adresseCli}</td>
                      <td>{item.telCli}</td>
                      <td>
                          <button  class="btn btn-primary"  onClick={(e)=> this.getClientById(e,  item.id, item.nomCli , item.adresseCli , item.telCli)} data-toggle="modal" data-target="#editModal" ><i class="fa fa-edit"></i> </button>
                          <button  class="btn btn-danger"   onClick={(e)=> this.getClientById(e,  item.id, item.nomCli , item.adresseCli , item.telCli)} data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash"></i> </button>

                      </td>

                  </tr>

              );
          });

      }



    return (

    <div>
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-600">Client</h1>
        <a onClick={this.initializeState}  href="#" data-toggle="modal" data-target="#addModal" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" ><i class="fas fa-compress fa-sm text-white-100"></i> Nouveau client</a>
      </div>


        <div class="card shadow mb-4">

            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">liste Client</h6>
            </div>

            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped dataTable" id="dataTable" width="100%" cellSpacing="0">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Nom</th>
                      <th>Adresse</th>
                      <th>Telephone</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  
                  <tbody>
                    {client_HTML_TABLE}

                  </tbody>
                </table>

              </div>
            </div>

        </div>


        <div class="modal fade" id="addModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Ajouter nouveau client</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <form onSubmit={this.saveClient}>
              <div class="modal-body">
                <input type="text" onChange={this.handleInput} name="nomCli" value={this.state.nomCli}  class="form-control" placeholder="Nom du client" /> <br />
                <input type="text" onChange={this.handleInput} name="adresseCli" value={this.state.adresseCli}  class="form-control" placeholder="adresse du client" /><br />            
                <input type="text" onChange={this.handleInput} name="telCli" value={this.state.telCli}  class="form-control" placeholder="Telephone client" />
              </div>
              <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Retour</button>
                <button type="submit" class="btn btn-primary" >Enregistrer</button>
              </div>
            </form>
          </div>
        </div>
      </div>


      <div class="modal fade" id="editModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Edition client</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <form >
            <div class="modal-body">
                <input type="text" onChange={this.handleInput} name="nomCli" value={this.state.nomCli}  class="form-control" placeholder="Nom du client" /> <br />
                <input type="text" onChange={this.handleInput} name="adresseCli" value={this.state.adresseCli}  class="form-control" placeholder="adresse du client" /><br />            
                <input type="text" onChange={this.handleInput} name="telCli" value={this.state.telCli}  class="form-control" placeholder="Telephone client" />
              </div>
              <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Retour</button>
                <button  onClick={(e)=> this.updateClient(e,this.state.idCli)} class="btn btn-primary" data-dismiss="modal">Enregistrer</button>
              </div>
            </form>
          </div>
        </div>
      </div>

       <div class="modal fade" id="deleteModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Suppression client</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <form >
              <div class="modal-body">
                  <p>Voulez vous vraiment supprimer le <strong>  client {this.state.nomCli} , ID: {this.state.idCli} </strong> ?<br/> Attention !!! Toutes attentions tous les données de ce produit vont etre supprimer </p>
              </div>
              <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Annuler</button>
                <button onClick={(e)=> this.deleteClient(e,this.state.idCli)}  class="btn btn-danger" data-dismiss="modal">Confirmer</button>
              </div>
            </form>
          </div>
        </div>
      </div>







    </div>

    );
  }
}
